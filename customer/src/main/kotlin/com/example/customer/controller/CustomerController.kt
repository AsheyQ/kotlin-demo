package com.example.customer.controller

import com.example.customer.dao.CustomerRegistrationRequest
import com.example.customer.dao.CustomerSearchDao
import com.example.customer.dao.CustomerSearchRequest
import com.example.customer.domain.Customer
import com.example.customer.service.CustomerService
import lombok.extern.slf4j.Slf4j
import mu.KotlinLogging
import org.springframework.web.bind.annotation.*

@Slf4j
@RestController
@RequestMapping("/customers")
class CustomerController(
    private val customerDao: CustomerSearchDao,
    private val customerService: CustomerService
) {
    private val logger = KotlinLogging.logger {}

    @GetMapping("/search")
    fun criteriaSearch(
        @RequestParam(required = false) firstName: String?,
        @RequestParam(required = false) lastName: String?,
        @RequestParam(required = false) email: String?
    ): List<Customer> {
        return customerDao.findAllByCriteria(CustomerSearchRequest(firstName, lastName, email))
    }

    @PostMapping("/register")
    fun registerCustomer(@RequestBody customerRegistrationRequest: CustomerRegistrationRequest): String {
        logger.info { "new customer registration $customerRegistrationRequest" }
        return customerService.registerCustomer(customerRegistrationRequest)
    }
}