package com.example.customer.dao

import com.example.customer.domain.Customer
import jakarta.persistence.EntityManager
import jakarta.persistence.TypedQuery
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root
import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Repository

@Repository
@RequiredArgsConstructor
class CustomerSearchDao(
    private val em: EntityManager
) {

    fun findAllBySimpleQuery(
        firstName: String,
        lastName: String,
        email: String
    ): List<Customer> {

        val criteriaBuilder = em.criteriaBuilder
        val criteriaQuery = criteriaBuilder.createQuery(Customer::class.java)

        //select * from employee
        val root: Root<Customer> = criteriaQuery.from(Customer::class.java)

        //WHERE
        val firstNamePredicate: Predicate = criteriaBuilder
            .like(root.get("firstName"), "%$firstName%")
        val lastNamePredicate: Predicate = criteriaBuilder
            .like(root.get("lastName"), "%$lastName%")
        val emailPredicate: Predicate = criteriaBuilder
            .like(root.get("email"), "%$email%")

        val firstNameOrLastnamePredicate: Predicate = criteriaBuilder
            .or(firstNamePredicate, lastNamePredicate)

        // final query select * from employee where firstName like __
        // or lastName like __ and email like __
        val andEmailPredicate = criteriaBuilder.and(firstNameOrLastnamePredicate, emailPredicate)
        criteriaQuery.where(andEmailPredicate)

        val query: TypedQuery<Customer> = em.createQuery(criteriaQuery)

        return query.resultList
    }

    fun findAllByCriteria(
        request: CustomerSearchRequest
    ): List<Customer> {
        val criteriaBuilder = em.criteriaBuilder
        val criteriaQuery = criteriaBuilder.createQuery(Customer::class.java)
        val predicates: ArrayList<Predicate> = ArrayList()

        val root = criteriaQuery.from(Customer::class.java)
        if (request.firstName != null) {
            val firstNamePredicate = criteriaBuilder
                .like(root.get("firstName"), "%${request.firstName}%")
            predicates.add(firstNamePredicate)
        }

        if (request.lastName != null) {
            val lastNamePredicate = criteriaBuilder
                .like(root.get("lastName"), "%${request.lastName}%")
            predicates.add(lastNamePredicate)
        }

        if (request.email != null) {
            val emailPredicate = criteriaBuilder
                .like(root.get("email"), "%${request.email}%")
            predicates.add(emailPredicate)
        }

        criteriaQuery.where(
            criteriaBuilder.or(*predicates.toTypedArray())
        )

        val query = em.createQuery(criteriaQuery)
        return query.resultList
    }
}