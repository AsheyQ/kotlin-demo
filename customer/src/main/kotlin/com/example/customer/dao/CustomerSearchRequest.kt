package com.example.customer.dao

data class CustomerSearchRequest(
    val firstName: String?,
    val lastName: String?,
    val email: String?
)