package com.example.customer.dao

data class CustomerRegistrationRequest(
    val firstName: String,
    val lastName: String,
    val email: String
)