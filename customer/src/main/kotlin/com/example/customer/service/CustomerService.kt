package com.example.customer.service

import com.example.clients.fraud.FraudClient
import com.example.clients.notification.NotificationClient
import com.example.clients.notification.NotificationMessage
import com.example.customer.dao.CustomerRegistrationRequest
import com.example.customer.dao.CustomerRepository
import com.example.customer.domain.Customer
import org.springframework.stereotype.Service

@Service
class CustomerService(
    private val fraudClient: FraudClient,
    private val notificationClient: NotificationClient,
    private val customerRepository: CustomerRepository
) {
    fun registerCustomer(customerRegistrationRequest: CustomerRegistrationRequest): String {
        val customer = Customer(
            firstName = customerRegistrationRequest.firstName,
            lastName = customerRegistrationRequest.lastName,
            email = customerRegistrationRequest.email
        )
        customerRepository.saveAndFlush(customer)
        val fraudResponse = fraudClient.isFraudster(customer.id)
        if (fraudResponse.isFraudster) {
            throw IllegalStateException("Registration denied, fraud customer")
        }

        val notificationMessage = NotificationMessage(
            toCustomerId = customer.id,
            toCustomerEmail = customer.email,
            message = "Hey ${customer.firstName}"
        )
        notificationClient.send(notificationMessage)

        return notificationMessage.message
    }
}