package com.example.customer.domain

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import java.util.*

@Entity
data class Customer(
    @Id
    @GeneratedValue
    var id: UUID? = null,

    var firstName: String,

    var lastName: String,

    var email: String,
)