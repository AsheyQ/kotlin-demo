package com.example.telegram.kafka

import com.example.telegram.service.TelegramService
import lombok.RequiredArgsConstructor
import lombok.extern.slf4j.Slf4j
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service
import ru.otpbank.kafka.selfconverter.KafkaNotificationTelegramSelfConverter


@Slf4j
@Service
@RequiredArgsConstructor
class KafkaListeners(private val telegramService: TelegramService) {

    @KafkaListener(
        topics = ["\${kafka.notification.telegram.topic}"],
        containerFactory = "telegramListenerContainerFactory",
        groupId = "telegram-listener"
    )
    fun listenNotification(notification: KafkaNotificationTelegramSelfConverter) {
        telegramService.sendMessage(notification)
    }
}
