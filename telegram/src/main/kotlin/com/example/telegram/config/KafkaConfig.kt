package com.example.telegram.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.task.SimpleAsyncTaskExecutor
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.config.KafkaListenerContainerFactory
import org.springframework.kafka.core.ConsumerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer
import org.springframework.kafka.support.serializer.JsonDeserializer
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor
import ru.otpbank.kafka.message.dto.NotificationTelegramDto

@Configuration
class KafkaConfig {

    @Bean("telegramConsumerFactory")
    fun telegramConsumerFactory(
        kafkaProperties: KafkaProperties, mapper: ObjectMapper?
    ): ConsumerFactory<String, NotificationTelegramDto> {
        val props: MutableMap<String, Any> = kafkaProperties.buildConsumerProperties()
        props[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java
        props[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] =
            JsonDeserializer::class.java
        props[ConsumerConfig.MAX_POLL_RECORDS_CONFIG] = 10
        props[ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG] = 3000
        val kafkaConsumerFactory: DefaultKafkaConsumerFactory<String, NotificationTelegramDto> =
            DefaultKafkaConsumerFactory<String, NotificationTelegramDto>(props)
        kafkaConsumerFactory.setValueDeserializer(JsonDeserializer<NotificationTelegramDto>(mapper))
        return kafkaConsumerFactory
    }

    @Bean("telegramListenerContainerFactory")
    fun listenerContainerFactoryTelegram(@Qualifier("telegramConsumerFactory") consumerFactory: ConsumerFactory<String?, NotificationTelegramDto?>): KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, NotificationTelegramDto>> {
        val factory: ConcurrentKafkaListenerContainerFactory<String, NotificationTelegramDto> =
            ConcurrentKafkaListenerContainerFactory<String, NotificationTelegramDto>()

        factory.consumerFactory = consumerFactory
        factory.isBatchListener = true
        factory.setConcurrency(1)
        factory.containerProperties.idleBetweenPolls = 1000
        factory.containerProperties.pollTimeout = 1000
        val executor = SimpleAsyncTaskExecutor("k-consumer-email-")
        executor.concurrencyLimit = 10
        val listenerTaskExecutor = ConcurrentTaskExecutor(executor)
        factory.containerProperties.listenerTaskExecutor = listenerTaskExecutor
        return factory
    }
}
