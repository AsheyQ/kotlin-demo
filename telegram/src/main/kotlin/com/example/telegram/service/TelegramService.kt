package com.example.telegram.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import ru.otpbank.kafka.selfconverter.KafkaNotificationTelegramSelfConverter

@Service
class TelegramService(
    @Value("\${telegram.bot-token}") private val botToken: String,
    @Value("\${telegram.chat-id}") private val chatId: String
) {

    private val restTemplate = RestTemplate()
    private val telegramApiBaseUrl = "https://api.telegram.org/bot"

    fun sendMessage(message: KafkaNotificationTelegramSelfConverter) {
        val url = "${telegramApiBaseUrl}$botToken/sendMessage"
        val body = message.items.first()
        val params = mapOf(
            "chat_id" to chatId, "text" to "CONSUMED NEW MESSAGE \n UserId: ${body?.toCustomerId} \n UserEmail: ${body?.toCustomerEmail}" +
                    "\n\n /Metadata/ \n\n $message"
        )

        restTemplate.postForObject(url, params, String::class.java)
    }
}