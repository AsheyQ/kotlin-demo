package ru.otpbank.kafka.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.kafka.support.converter.BatchMessagingMessageConverter;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import ru.otpbank.kafka.interceptor.LoggingRawJsonKafkaRecordInterceptor;
import ru.otpbank.kafka.message.AbstractMessage;
import ru.otpbank.kafka.sender.CustomProducerListener;
import ru.otpbank.kafka.serializer.CustomJsonDeserializer;
import ru.otpbank.kafka.serializer.CustomJsonSerializer;

@Configuration
@RequiredArgsConstructor
@SuppressWarnings("JavadocMethod")
public class KafkaConfiguration {

    private static final int MAX_REQUEST_SIZE = 50 * 1024 * 1024;

    private final MeterRegistry meterRegistry;
    private final KafkaProperties kafkaProperties;

    @Bean
    @Primary
    public StringJsonMessageConverter stringJsonMessageConverter() {
        return new StringJsonMessageConverter(createKafkaObjectMapper());
    }

    @Bean
    public BatchMessagingMessageConverter batchMessagingMessageConverter() {
        return new BatchMessagingMessageConverter(stringJsonMessageConverter());
    }

    @Bean
    public ProducerListener<String, AbstractMessage<?>>  customProducerListener() {
        return new CustomProducerListener(meterRegistry);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, AbstractMessage<?>> kafkaBatchListenerContainerFactory(
            ConsumerFactory<String, AbstractMessage<?>> consumerFactory
    ) {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, AbstractMessage<?>>();
        factory.setConsumerFactory(consumerFactory);
        factory.setBatchListener(true);
        factory.setBatchMessageConverter(batchMessagingMessageConverter());
        return factory;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, AbstractMessage<?>> kafkaListenerContainerFactory(
            ConsumerFactory<String, AbstractMessage<?>> consumerFactory
    ) {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, AbstractMessage<?>>();
        factory.setConsumerFactory(consumerFactory);
        factory.setRecordMessageConverter(stringJsonMessageConverter());
        factory.setRecordInterceptor(new LoggingRawJsonKafkaRecordInterceptor<>(createKafkaObjectMapper()));
        return factory;
    }

    @Bean
    public ProducerFactory<String, AbstractMessage<?>> kafkaProducerFactory(
            @Qualifier("kafkaObjectMapper") ObjectMapper kafkaObjectMapper) {
        var producerProps = kafkaProperties.buildProducerProperties();
        var objectSerializer = new CustomJsonSerializer(kafkaObjectMapper);
        objectSerializer.setAddTypeInfo(false);
        producerProps.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, MAX_REQUEST_SIZE);
        return new DefaultKafkaProducerFactory<>(producerProps, new StringSerializer(), objectSerializer);
    }

    @Bean
    @Primary
    public KafkaTemplate<?, ?> kafkaTemplate(RecordMessageConverter messageConverter,
                                             ProducerFactory<String, AbstractMessage<?>> kafkaProducerFactory,
                                             ProducerListener<String, AbstractMessage<?>> customProducerListener) {
        var kafkaTemplate = new KafkaTemplate<>(kafkaProducerFactory);
        if (messageConverter != null) {
            kafkaTemplate.setMessageConverter(messageConverter);
        }
        kafkaTemplate.setProducerListener(customProducerListener);
        return kafkaTemplate;
    }

    @Bean("kafkaObjectMapper")
    public ObjectMapper createKafkaObjectMapper() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
                .deactivateDefaultTyping();
    }


    @Bean
    public KafkaListenerContainerFactory<?> singleFactory(@Qualifier("kafkaObjectMapper") ObjectMapper objectMapper,
                                                          ConsumerFactory<String, AbstractMessage<?>> consumerFactory) {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, AbstractMessage<?>>();
        factory.setConsumerFactory(consumerFactory);
        factory.setBatchListener(false);
        factory.setRecordInterceptor(new LoggingRawJsonKafkaRecordInterceptor<>(objectMapper));
        return factory;
    }

    @Bean
    public ConsumerFactory<String, AbstractMessage<?>> consumerFactory(@Qualifier("kafkaObjectMapper")ObjectMapper objectMapper) {
        var consumerProps = kafkaProperties.buildConsumerProperties();
        var valueDeserializer = new CustomJsonDeserializer();
        var keyDeserializer = new StringDeserializer();
        valueDeserializer.setObjectMapper(objectMapper);
        consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerProps.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);

        return new DefaultKafkaConsumerFactory<>(consumerProps, keyDeserializer, valueDeserializer);
    }
}
