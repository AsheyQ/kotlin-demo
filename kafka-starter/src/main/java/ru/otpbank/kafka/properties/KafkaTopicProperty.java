package ru.otpbank.kafka.properties;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class KafkaTopicProperty {
    private String topic;
    private boolean enabled;
}
