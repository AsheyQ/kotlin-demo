package ru.otpbank.kafka.type;

public enum EmailMessageTemplateEnum {
    EMAIL_REQUISITES_RU_SEND,
    EMAIL_REQUISITES_EN_SEND,
    EMAIL_CHANGE_OTP_SEND,
    EMAIL_CHANGE_NOTIF_SEND

}
