package ru.otpbank.kafka.type;

public enum SmsMessageTemplateEnum {
    TWO_FA,
    USER_REGISTRATION,
    SIGN_DOCUMENT,
    CHANGE_EMAIL_SIGN,
    CHANGE_PHONE_SIGN,
    CHANGE_NEW_PHONE_OTP,
    RESET_PASSWORD,
    CHANGE_LOGIN_SIGN,
    CHANGE_PASSWORD_SIGN

}
