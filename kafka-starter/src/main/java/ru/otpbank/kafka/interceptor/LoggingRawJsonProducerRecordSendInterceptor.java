package ru.otpbank.kafka.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;

@Slf4j
@RequiredArgsConstructor
public class LoggingRawJsonProducerRecordSendInterceptor<K, V> implements ProducerRecordSendInterceptor<K, V> {

    private final ObjectMapper objectMapper;

    @Override
    @SneakyThrows
    public ProducerRecord<K, V> intercept(ProducerRecord<K, V> producerRecord) {
        final var jsonValue = objectMapper.writeValueAsString(producerRecord.value());

        log.info(
            "Sended message: topic:{} key:{} message: {}",
            producerRecord.topic(),
            producerRecord.key(),
            jsonValue
        );
        return producerRecord;
    }
}
