package ru.otpbank.kafka.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.RecordInterceptor;

@Slf4j
@RequiredArgsConstructor
public class LoggingRawJsonKafkaRecordInterceptor<K, V> implements RecordInterceptor<K, V> {

    private final ObjectMapper objectMapper;

    @Override
    @SneakyThrows
    public ConsumerRecord<K, V> intercept(ConsumerRecord<K, V> consumerRecord,  Consumer<K, V> consumer ) {

        final var jsonValue = objectMapper.writeValueAsString(consumerRecord.value());

        log.info(
            "Received message: topic:{} key:{} message: {}",
            consumerRecord.topic(),
            consumerRecord.key(),
            jsonValue
        );
        return consumerRecord;
    }


}
