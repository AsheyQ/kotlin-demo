package ru.otpbank.kafka.interceptor;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.lang.Nullable;

@FunctionalInterface
public interface ProducerRecordSendInterceptor<K, V> {

    /**
     * Intercepts producer record before send.
     *
     * @param producerRecord - message
     * @return message after processing
     */
    @Nullable
    ProducerRecord<K, V> intercept(ProducerRecord<K, V> producerRecord);
}
