package ru.otpbank.kafka.sender;

import org.apache.kafka.common.header.Header;

public interface KafkaMessageSender<K, V> {

    /**
     * Отправка сообщения.
     *
     * @param message сообщение
     */
    void send(V message);

    /**
     * Отправка сообщения.
     *
     * @param key     ключ сообщения
     * @param message сообщение
     */
    void send(K key, V message);

    /**
     * Отправка сообщения.
     *
     * @param key     ключ сообщения
     * @param message сообщение
     * @param headers заголовки сообщения
     */
    void send(K key, V message, Iterable<Header> headers);
}
