package ru.otpbank.kafka.sender;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.support.ProducerListener;
import ru.otpbank.kafka.message.AbstractMessage;

public class CustomProducerListener implements ProducerListener<String, AbstractMessage<?>> {

    private final Counter successfulMessagesCounter;
    private final Counter errorMessagesCounter;

    public CustomProducerListener(MeterRegistry meterRegistry) {
        this.successfulMessagesCounter = Counter.builder("kafka.producer.successful.messages")
            .description("Number of successful messages sent to Kafka")
            .register(meterRegistry);
        this.errorMessagesCounter = Counter.builder("kafka.producer.error.messages")
            .description("Number of error messages encountered while sending to Kafka")
            .register(meterRegistry);
    }

    @Override
    public void onSuccess(ProducerRecord<String, AbstractMessage<?>> producerRecord, RecordMetadata recordMetadata) {
        successfulMessagesCounter.increment();
    }

    @Override
    public void onError(ProducerRecord<String, AbstractMessage<?>> producerRecord, RecordMetadata recordMetadata, Exception exception) {
        errorMessagesCounter.increment();
    }

}