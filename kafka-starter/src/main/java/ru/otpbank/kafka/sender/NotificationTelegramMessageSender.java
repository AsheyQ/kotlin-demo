package ru.otpbank.kafka.sender;

import ru.otpbank.kafka.message.AbstractMessage;

public interface NotificationTelegramMessageSender<V> extends KafkaMessageSender<String, AbstractMessage<V>>{
}
