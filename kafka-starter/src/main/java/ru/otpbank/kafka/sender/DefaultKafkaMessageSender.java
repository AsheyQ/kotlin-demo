package ru.otpbank.kafka.sender;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.CollectionUtils;
import ru.otpbank.kafka.interceptor.ProducerRecordSendInterceptor;
import ru.otpbank.kafka.message.AbstractMessage;

import java.util.Optional;

@Slf4j
@Getter(AccessLevel.PROTECTED)
@RequiredArgsConstructor
public class DefaultKafkaMessageSender<V> implements DboKafkaMessageSender<V>, NotificationTelegramMessageSender<V> {

    private final KafkaTemplate<Object, Object> kafkaTemplate;
    private final ProducerRecordSendInterceptor<String, AbstractMessage<V>> recordInterceptor;

    protected String topicName;

    protected boolean enabled;


    /**
     * Отправка сообщения.
     *
     * @param message сообщение
     */
    public void send(AbstractMessage<V> message) {
        send(null, message);
    }

    /**
     * Отправка сообщения.
     *
     * @param key     ключ сообщения
     * @param message сообщение
     */
    public void send(String key, AbstractMessage<V> message) {
        send(key, message, new RecordHeaders());
    }

    @Override
    public synchronized void send(String key, AbstractMessage<V> message, Iterable<Header> headers) {
        if (!CollectionUtils.isEmpty(message.getItems()) && isEnabled()) {
            ProducerRecord<String, AbstractMessage<V>> record = new ProducerRecord<>(topicName,null, key, message, headers);
            ProducerRecord finalRecord = Optional.ofNullable(recordInterceptor)
                    .map(interceptor -> interceptor.intercept(record))
                    .orElse(record);
            kafkaTemplate.send(finalRecord);
        }
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
