package ru.otpbank.kafka;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.otpbank.kafka.config.KafkaConfiguration;

@Configuration
@Import(KafkaConfiguration.class)
@AutoConfigureBefore(KafkaAutoConfiguration.class)
public class DboKafkaAutoConfiguration {
    //todo: нужно обработать логи интерсептеров
}
