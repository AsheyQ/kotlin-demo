package ru.otpbank.kafka.message.dto;

import ru.otpbank.kafka.type.SmsMessageTemplateEnum;

import java.util.Map;
import java.util.UUID;

public record NotificationSmsDto(String phone,
                                 SmsMessageTemplateEnum smsMessageTemplateEnum,
                                 Map<String, String> attributes,
                                 UUID personId) {
}
