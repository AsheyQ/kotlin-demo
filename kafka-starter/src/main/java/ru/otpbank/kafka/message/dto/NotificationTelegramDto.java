package ru.otpbank.kafka.message.dto;

import java.util.UUID;

public record NotificationTelegramDto(UUID toCustomerId,
                                      String toCustomerEmail) {
}
