package ru.otpbank.kafka.message.dto;

import ru.otpbank.kafka.type.EmailMessageTemplateEnum;

import java.util.Map;
import java.util.UUID;

public record NotificationEmailDto(String email,
                                   EmailMessageTemplateEnum emailMessageTemplateEnum,
                                   Map<String, String> attributes,
                                   UUID personId) {
}
