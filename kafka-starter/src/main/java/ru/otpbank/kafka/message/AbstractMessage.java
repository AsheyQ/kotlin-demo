package ru.otpbank.kafka.message;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
public class AbstractMessage<T> implements Serializable {
    private static final long serialVersionUID = 5895988552416948726L;
    @JsonFormat(
        pattern = "yyyy-MM-dd'T'HH:mm:ss",
        shape = Shape.STRING
    )
    protected LocalDateTime creationDate;
    protected String messageId;
    protected String processId;
    private Set<T> items;
    private Set<T> removedItems;

    @JsonIgnore
    protected Class<?> type;

    protected AbstractMessage(LocalDateTime creationDate, String messageId, Set<T> items) {
        this.creationDate = creationDate != null ? creationDate : LocalDateTime.now();
        this.messageId = messageId != null ? messageId : UUID.randomUUID().toString();
        this.items = (Set)(!CollectionUtils.isEmpty(items) ? items : new HashSet());
        setType(items, null);
    }

    public AbstractMessage(LocalDateTime creationDate, String messageId, Set<T> items, Set<T> removedItems) {
        this(creationDate, messageId, items);
        this.removedItems = (Set)(!CollectionUtils.isEmpty(removedItems) ? removedItems : new HashSet());
        setType(items, removedItems);
    }

    public AbstractMessage(LocalDateTime creationDate, String messageId, Set<T> items, Set<T> removedItems, String processId) {
        this(creationDate, messageId, items, removedItems);
        this.processId = processId;
        setType(items, removedItems);
    }

    public void setItems(Set<T> items){
        this.items = items;
        setType(items, null);
    }

    public void setRemovedItems(Set<T> removedItems) {
        this.removedItems = removedItems;
        setType(null, removedItems);
    }

    private void setType(Set<T> items, Set<T> removedItems) {
        if(type == null) {
            if (!CollectionUtils.isEmpty(items)) {
                this.type = items.stream().findAny().get().getClass();
            } else if (!CollectionUtils.isEmpty(removedItems)) {
                this.type = removedItems.stream().findAny().getClass();
            }
        }
    }

    public String toString() {
        LocalDateTime var10000 = this.getCreationDate();
        return "AbstractMessage(creationDate=" + var10000 + ", messageId=" + this.getMessageId() + ", processId=" + this.getProcessId() + ", items=" + this.getItems() + ", removedItems=" + this.getRemovedItems() + ")";
    }

    protected AbstractMessage() {
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof AbstractMessage)) {
            return false;
        } else {
            AbstractMessage<?> other = (AbstractMessage)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label71: {
                    Object this$creationDate = this.getCreationDate();
                    Object other$creationDate = other.getCreationDate();
                    if (this$creationDate == null) {
                        if (other$creationDate == null) {
                            break label71;
                        }
                    } else if (this$creationDate.equals(other$creationDate)) {
                        break label71;
                    }

                    return false;
                }

                Object this$messageId = this.getMessageId();
                Object other$messageId = other.getMessageId();
                if (this$messageId == null) {
                    if (other$messageId != null) {
                        return false;
                    }
                } else if (!this$messageId.equals(other$messageId)) {
                    return false;
                }

                label57: {
                    Object this$processId = this.getProcessId();
                    Object other$processId = other.getProcessId();
                    if (this$processId == null) {
                        if (other$processId == null) {
                            break label57;
                        }
                    } else if (this$processId.equals(other$processId)) {
                        break label57;
                    }

                    return false;
                }

                Object this$items = this.getItems();
                Object other$items = other.getItems();
                if (this$items == null) {
                    if (other$items != null) {
                        return false;
                    }
                } else if (!this$items.equals(other$items)) {
                    return false;
                }

                Object this$removedItems = this.getRemovedItems();
                Object other$removedItems = other.getRemovedItems();
                if (this$removedItems == null) {
                    if (other$removedItems == null) {
                        return true;
                    }
                } else if (this$removedItems.equals(other$removedItems)) {
                    return true;
                }

                return false;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof AbstractMessage;
    }

    public int hashCode() {
        boolean PRIME = true;
        int result = 1;
        Object $creationDate = this.getCreationDate();
        result = result * 59 + ($creationDate == null ? 43 : $creationDate.hashCode());
        Object $messageId = this.getMessageId();
        result = result * 59 + ($messageId == null ? 43 : $messageId.hashCode());
        Object $processId = this.getProcessId();
        result = result * 59 + ($processId == null ? 43 : $processId.hashCode());
        Object $items = this.getItems();
        result = result * 59 + ($items == null ? 43 : $items.hashCode());
        Object $removedItems = this.getRemovedItems();
        result = result * 59 + ($removedItems == null ? 43 : $removedItems.hashCode());
        return result;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }
}
