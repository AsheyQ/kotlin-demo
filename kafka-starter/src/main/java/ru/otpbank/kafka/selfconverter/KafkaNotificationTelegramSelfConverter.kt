package ru.otpbank.kafka.selfconverter

import ru.otpbank.kafka.message.AbstractMessage
import ru.otpbank.kafka.message.dto.NotificationTelegramDto
import ru.otpbank.kafka.reflection.HeaderTypeReference
import ru.otpbank.kafka.type.SmsMessageTemplateEnum
import java.util.*
import java.util.Set

@HeaderTypeReference(NotificationTelegramDto::class)
class KafkaNotificationTelegramSelfConverter(
    type: SmsMessageTemplateEnum?,
    phone: String?,
    otp: String?,
    personId: UUID?,
    email: String?
) : AbstractMessage<NotificationTelegramDto?>(
    null, null,
    Set.of(
        NotificationTelegramDto(
            personId, email
        )
    )
)
