package ru.otpbank.kafka.serializer;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;
import org.springframework.util.Assert;
import ru.otpbank.kafka.message.AbstractMessage;

import java.nio.charset.StandardCharsets;
import java.util.Map;


public class CustomJsonDeserializer implements Deserializer<AbstractMessage<?>> {

    private static final String EXCEPTION_MESSAGE = "Deserialization exception: ";
    private static final String VALUE_TYPE_REF = "_valueTypeRef";

    private ObjectMapper objectMapper;

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        Deserializer.super.configure(configs, isKey);
    }

    @Override
    public AbstractMessage<?> deserialize(String topic, byte[] data) {
        return null;
    }

    @Override
    @SneakyThrows
    public AbstractMessage<?> deserialize(String topic, Headers headers, byte[] data) {
        String targetClassRef = new String(headers.lastHeader(VALUE_TYPE_REF).value(), StandardCharsets.UTF_8);
        Assert.state(!targetClassRef.isEmpty(), EXCEPTION_MESSAGE + "value type reference is null. Add type header to message");
        Class<?> targetClass = getTargetClassInClasspath(targetClassRef);
        JavaType constructedType = objectMapper.getTypeFactory().constructParametricType(AbstractMessage.class, targetClass);
        return objectMapper.readValue(data, constructedType);
    }

    @Override
    public void close() {
        Deserializer.super.close();
    }

    private Class<?> getTargetClassInClasspath(String classRef) {
        try {
            return Class.forName(classRef);
        } catch (ClassNotFoundException e) {
            throw new SerializationException(EXCEPTION_MESSAGE + "target type is not found. Check type reference");
        }
    }
}
