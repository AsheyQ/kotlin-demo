package ru.otpbank.kafka.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.springframework.kafka.support.serializer.JsonSerializer;
import ru.otpbank.kafka.message.AbstractMessage;
import ru.otpbank.kafka.reflection.HeaderTypeReference;

import java.nio.charset.StandardCharsets;

public class CustomJsonSerializer extends JsonSerializer<AbstractMessage<?>> {

    private static final String DEFAULT_TYPE_REF_HEADER = "_valueTypeRef";

    private boolean addValueTypeRef = true;

    public void setAddValueTypeRef(boolean addValueTypeRef) {
        this.addValueTypeRef = addValueTypeRef;
    }

    @Override
    public byte[] serialize(String topic, Headers headers, AbstractMessage<?> data) {
        if(headers == null){
            headers = new RecordHeaders();
        }
        if(addValueTypeRef){
            headers.add(getTypeReferenceHeader(data));
        }
        return super.serialize(topic, headers, data);
    }

    @Override
    public byte[] serialize(String topic, AbstractMessage<?> data) {
        return super.serialize(topic, data);
    }

    public CustomJsonSerializer(ObjectMapper objectMapper){
        super(objectMapper);
    }

    private Header getTypeReferenceHeader(AbstractMessage<?> message) {
        if (message.getClass().isAnnotationPresent(HeaderTypeReference.class)) {
            var typeRefer = message.getClass().getAnnotation(HeaderTypeReference.class);
            return new RecordHeader(typeRefer.name(), typeRefer.value().getName().getBytes(StandardCharsets.UTF_8));
        } else if (message.getType() != null) {
            return new RecordHeader(DEFAULT_TYPE_REF_HEADER, message.getType().getName().getBytes(StandardCharsets.UTF_8));
        }
        return new RecordHeader(DEFAULT_TYPE_REF_HEADER, Object.class.getName().getBytes(StandardCharsets.UTF_8));
    }
}
