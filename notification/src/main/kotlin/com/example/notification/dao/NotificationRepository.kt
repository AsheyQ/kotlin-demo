package com.example.notification.dao

import com.example.notification.domain.Notification
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface NotificationRepository : JpaRepository<Notification, UUID>