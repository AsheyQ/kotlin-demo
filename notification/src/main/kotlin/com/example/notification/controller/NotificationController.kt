package com.example.notification.controller

import com.example.clients.notification.NotificationMessage
import com.example.notification.service.NotificationService
import lombok.extern.slf4j.Slf4j
import mu.KotlinLogging
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Slf4j
@RestController
@RequestMapping("/notification")
class NotificationController(
    private val notificationService: NotificationService
) {
    private val logger = KotlinLogging.logger {}

    @PostMapping("/send")
    fun send(@RequestBody notificationMessage: NotificationMessage) {
        logger.info("new notification $notificationMessage")
        notificationService.send(notificationMessage)
    }
}