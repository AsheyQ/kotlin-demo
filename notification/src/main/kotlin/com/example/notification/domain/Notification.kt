package com.example.notification.domain

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import java.time.LocalDateTime
import java.util.*

@Entity
data class Notification(
    @Id
    @GeneratedValue
    var id: UUID? = null,

    var toCustomerId: UUID?,

    var toCustomerEmail: String,

    var sender: String? = "Kotlin",

    var message: String,

    var sentAt: LocalDateTime = LocalDateTime.now()
)
