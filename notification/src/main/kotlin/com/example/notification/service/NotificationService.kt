package com.example.notification.service

import com.example.clients.notification.NotificationMessage
import com.example.notification.dao.NotificationRepository
import com.example.notification.domain.Notification
import org.springframework.stereotype.Service
import ru.otpbank.kafka.message.dto.NotificationTelegramDto
import ru.otpbank.kafka.selfconverter.KafkaNotificationTelegramSelfConverter
import ru.otpbank.kafka.sender.NotificationTelegramMessageSender
import ru.otpbank.kafka.type.SmsMessageTemplateEnum

@Service
class NotificationService(
    private val notificationRepository: NotificationRepository,
    private val telegramSender: NotificationTelegramMessageSender<NotificationTelegramDto>
) {
    fun send(notificationMessage: NotificationMessage) {
        val message = Notification(
            toCustomerId = notificationMessage.toCustomerId,
            toCustomerEmail = notificationMessage.toCustomerEmail,
            message = notificationMessage.message
        )
        notificationRepository.save(message)

        telegramSender.send(
            KafkaNotificationTelegramSelfConverter(
                SmsMessageTemplateEnum.USER_REGISTRATION,
                "phone",
                "otp",
                notificationMessage.toCustomerId,
                notificationMessage.toCustomerEmail
            )
        )
    }
}