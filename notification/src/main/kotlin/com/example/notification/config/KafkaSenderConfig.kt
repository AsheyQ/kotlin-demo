package com.example.notification.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.core.KafkaTemplate
import ru.otpbank.kafka.interceptor.LoggingRawJsonProducerRecordSendInterceptor
import ru.otpbank.kafka.message.AbstractMessage
import ru.otpbank.kafka.message.dto.NotificationTelegramDto
import ru.otpbank.kafka.properties.KafkaTopicProperty
import ru.otpbank.kafka.sender.DefaultKafkaMessageSender
import ru.otpbank.kafka.sender.NotificationTelegramMessageSender

@Configuration
class KafkaSenderConfig {

    @Bean
    @ConfigurationProperties("kafka.notification.telegram")
    fun notificationToTelegramKafkaTopicProperty(): KafkaTopicProperty {
        return KafkaTopicProperty()
    }

    @Bean
    fun telegramSender(
        @Qualifier("notificationToTelegramKafkaTopicProperty") kafkaTopicProperty: KafkaTopicProperty,
        kafkaTemplate: KafkaTemplate<Any?, Any?>?,
        objectMapper: ObjectMapper?
    ): NotificationTelegramMessageSender<NotificationTelegramDto> {
        val sender = DefaultKafkaMessageSender(
            kafkaTemplate,
            LoggingRawJsonProducerRecordSendInterceptor<String, AbstractMessage<NotificationTelegramDto>>(objectMapper)
        )
        sender.setEnabled(kafkaTopicProperty.isEnabled)
        sender.setTopicName(kafkaTopicProperty.topic)
        return sender
    }
}
