package com.example.fraud.domain

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import java.time.LocalDateTime
import java.util.*

@Entity
data class FraudCheckHistory(
    @Id
    @GeneratedValue
    var id: UUID? = null,

    var customerId: String,

    var isFraudster: Boolean,

    var createdAt: LocalDateTime = LocalDateTime.now(),
)