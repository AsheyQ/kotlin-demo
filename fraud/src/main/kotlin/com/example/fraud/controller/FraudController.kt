package com.example.fraud.controller

import com.example.clients.fraud.FraudCheckResponse
import com.example.fraud.service.FraudService
import lombok.extern.slf4j.Slf4j
import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@Slf4j
@RestController
@RequestMapping("/fraud-check")
class FraudController(
    private val fraudService: FraudService
) {
    private val logger = KotlinLogging.logger {}

    @GetMapping(path = ["{customerId}"])
    fun isFraudster(
        @PathVariable("customerId") customerID: UUID
    ): FraudCheckResponse {
        val isFraudCustomer: Boolean = fraudService.isFraudCustomer(customerID)
        logger.info("fraud check request for customer $customerID")
        return FraudCheckResponse(isFraudCustomer)
    }
}