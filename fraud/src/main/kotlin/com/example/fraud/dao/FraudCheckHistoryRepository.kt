package com.example.fraud.dao

import com.example.fraud.domain.FraudCheckHistory
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*


interface FraudCheckHistoryRepository : JpaRepository<FraudCheckHistory, UUID>
