package com.example.fraud.service

import com.example.fraud.dao.FraudCheckHistoryRepository
import com.example.fraud.domain.FraudCheckHistory
import org.springframework.stereotype.Service
import java.util.*

@Service
class FraudService(
    private val fraudCheckHistoryRepository: FraudCheckHistoryRepository
) {

    fun isFraudCustomer(customerId: UUID): Boolean {
        fraudCheckHistoryRepository.save(
            FraudCheckHistory(customerId = customerId.toString(), isFraudster = false)
        )

        return false
    }
}