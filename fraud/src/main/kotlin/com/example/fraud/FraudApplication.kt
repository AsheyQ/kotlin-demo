package com.example.fraud

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.PropertySource
import org.springframework.context.annotation.PropertySources

@SpringBootApplication
class FraudApplication

fun main(args: Array<String>) {
    runApplication<FraudApplication>(*args)
}
