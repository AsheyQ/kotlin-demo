package com.example.clients.fraud

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import java.util.*

@FeignClient(name = "fraud", url = "\${clients.fraud.url}")
interface FraudClient {
    @GetMapping("fraud-check/{customerId}")
    fun isFraudster(@PathVariable customerId: UUID?): FraudCheckResponse
}