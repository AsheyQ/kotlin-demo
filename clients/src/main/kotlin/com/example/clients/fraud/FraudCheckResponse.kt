package com.example.clients.fraud

data class FraudCheckResponse(
    val isFraudster: Boolean
)