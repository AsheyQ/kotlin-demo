package com.example.clients.notification

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import java.util.*

@FeignClient(name = "notification", url = "\${clients.notification.url}")
interface NotificationClient {
    @PostMapping("notification/send")
    fun send(@RequestBody notificationMessage: NotificationMessage)
}