package com.example.clients.notification

import java.util.*

data class NotificationMessage(
    var toCustomerId: UUID?,
    var toCustomerEmail: String,
    var message: String
)
