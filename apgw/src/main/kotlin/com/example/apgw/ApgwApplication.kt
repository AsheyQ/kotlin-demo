package com.example.apgw

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ApgwApplication

fun main(args: Array<String>) {
    runApplication<ApgwApplication>(*args)
}
